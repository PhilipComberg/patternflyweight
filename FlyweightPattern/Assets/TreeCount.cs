﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeCount : MonoBehaviour
{

    public GameObject Terrain;
    public int NumberOfTrees;

    void Start()
    {
        NumberOfTrees = GetComponent<Terrain>().terrainData.treeInstanceCount;
    }

}