﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Item", menuName = "Items")]
[System.Serializable]
public class ItemInfo : ScriptableObject
{
   
    public string itemName;
    public string itemType;

    public Sprite itemIcon;

    public int damage;
    public float attackSpeed;

    public string description;

    public int goldValue;

}
