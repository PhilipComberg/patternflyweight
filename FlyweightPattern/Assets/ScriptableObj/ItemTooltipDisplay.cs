﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class ItemTooltipDisplay : MonoBehaviour
{

    public ItemInfo itemInfo;

    public Image iconImage;

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI itemTypeText;
    public TextMeshProUGUI damageText;
    public TextMeshProUGUI attackSpeedText;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI goldValueText;

    void Start()
    {

        iconImage.sprite = itemInfo.itemIcon;

        nameText.text = itemInfo.itemName;
        itemTypeText.text = itemInfo.itemType;
        damageText.text = "Dmg: " + itemInfo.damage.ToString();
        attackSpeedText.text = "APS: " + itemInfo.attackSpeed.ToString();
        descriptionText.text = itemInfo.description;
        goldValueText.text = itemInfo.goldValue.ToString() + " Gold";

    }

}
